// => nestjs TypeGraphql
import { Field, ID, InputType } from '@nestjs/graphql';

// => common class-validatior validations
import {
  IsNotEmpty,
  IsUUID 
} from 'class-validator';

// => our costum validation rules 
import { IssueExists } from "../validation/issueExists"


@InputType('IssueByIdInput', { description: 'Also ensures that the issue with the provided id exists.'})
export class IssueByIdArgs{
  @IssueExists()
  @IsUUID()
  @IsNotEmpty()
  @Field(() => ID)
  id: string;

}