// => nestjs TypeGraphql
import { Field, ID, InputType, Int } from '@nestjs/graphql';
import * as GraphQLUpload from 'graphql-upload/GraphQLUpload.js';
// => common class-validatior validations
import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsUUID,
  MaxLength, MinLength
} from 'class-validator';
import { Stream } from 'stream';

interface FileUpload {
  filename: string;
  mimetype: string;
  encoding: string;
  createReadStream: () => Stream;
}

// => our costum validators 
import { IsLargerThanZero } from '../validation/isPositiveNotZero';
import { generateIdentifier } from '../utils/generateIdentifier';
import { IssueColumnExists } from 'src/issueColumn/validators/issueColumnExists';
import { UserExists } from 'src/user/validation/userExists';
import { S3StoredFileExists } from 'src/s3StoredFile/validations/s3StoredFileExists';

@InputType('CreateIssueInput', { 
  description: 'Used in issue Creation'})

// Could not implement identifier

export class CreateIssueInput {

  @MaxLength(50)
  @MinLength(1)
  @IsNotEmpty()
  @Field(() => String, { nullable: false, description: "Title of the task."})
    title: string;

  @MaxLength(8)
  @MinLength(8)
  @Field(() => String, { nullable: true, description: "Unique identifier of issue.", defaultValue: generateIdentifier()})
    identifierCode:  string;

  @MaxLength(150)
  @MinLength(1)
  @IsNotEmpty()
  @Field(() => String, { nullable: false, description: "Describe the task in detail"})
    description: string;
   
  @MaxLength(50)
  @MinLength(1)
  @IsNotEmpty()
  @Field(() => String, { nullable: false, description: "Describe the task in human terms."})
    descriptionInHumanTerms: string;

  @IsInt()
  @IsLargerThanZero()
  @Field(() => Int, { nullable: true, description: "A number from 1 to n where 1 is the highest priority."})
    priority?: number; 

  // => foreign fields 

  @IsUUID()
  @IssueColumnExists()
  @IsNotEmpty()
  @Field(() => ID, { nullable: false, description: "ID of existent IssueColumn."})
    currentColumnId: string; 
 
  @IsUUID()
  @UserExists()
  @Field(() => ID, { nullable: true, description: "ID of existent User to assign this issue to."})
    assignedToId: string; 
  

  @Field(() => GraphQLUpload, { nullable: true, description: "Files to upload and attach to this issue."})
    attachedFiles: Promise<FileUpload>;
  
}
