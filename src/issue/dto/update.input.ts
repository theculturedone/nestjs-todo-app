// => nestjs TypeGraphql
import { Field, ID, InputType, PartialType } from '@nestjs/graphql';

// => input types 
import { CreateIssueInput } from './create.input';

// => common class-validatior validations
import {
  IsUUID,
} from 'class-validator';

// => our costum validation rules 
import { IssueExists } from "../validation/issueExists"


// ! we only specify the fields that are extra compared to the CreateIssueInput and we set the ones that are present
// inside the CreateIssueInput to optional by using PartialType(CreateIssueInput)
// less code better :D
@InputType('UpdateIssueInput', { description: 'Used in issue Updating'})
export class UpdateIssueInput extends PartialType(CreateIssueInput){
  
  @IssueExists()
  @IsUUID()
  @Field(() => ID, { nullable: false })
    id: string;

  }
