import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { IssueResolver } from './issue.resolver';
import { IssueService } from './issue.service';
import { IssueExistsRule } from './validation/issueExists';
import { Issue } from './models/issue.model';
import { S3StoredFileModule } from 'src/s3StoredFile/s3StoredFile.module';
import { UserModule } from 'src/user/user.module';
import { IssueColumModule } from 'src/issueColumn/issueColumn.module';
import { generateIdentifier } from './utils/generateIdentifier';

@Module({
  imports: [S3StoredFileModule, UserModule, IssueColumModule ],
  providers: [ IssueExistsRule, IssueService, PrismaService, IssueResolver,
    {
      provide: 'generateIdentifier',
      useValue: generateIdentifier,
  }],
  exports: [ IssueService, Issue ]
})
export class IssueModule {}
