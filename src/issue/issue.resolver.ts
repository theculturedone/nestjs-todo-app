// -> framework
import { Args, Context, GqlContextType, Mutation, Query, Resolver } from '@nestjs/graphql';

// -> models
import { Issue } from './models/issue.model';

// => services
import { IssueService } from './issue.service';

// => input types (query/mutation arguments)
import { IssueByIdArgs } from './dto/byId.input';
import { CreateIssueInput } from './dto/create.input';
import { UpdateIssueInput } from './dto/update.input';
import { createWriteStream } from 'fs';

@Resolver(() => Issue)
export class IssueResolver {
  constructor(private issueService: IssueService) {}

  // => Gets single issue by id
  @Query(() => Issue, { description: 'Get issue by id' })
  async todo(@Args('GetIssueInput') { id }: IssueByIdArgs): Promise<Issue> {
    return this.issueService.getOne({
      id: id,
    });
  }

  // => Gets all issues
  @Query(() => [Issue], { description: 'Gets all issues' })
  async todos(): Promise<Issue[]> {
    return await this.issueService.getMany({});
  }

  // => Creates new issue
  @Mutation(() => Issue, { description: 'Create new issue' })
  async createTodo(
    @Args('CreateIssueInput') data: CreateIssueInput, @Context() ctx: any): Promise<Issue> {
      const { filename, mimetype, encoding, createReadStream } = await data.attachedFiles;
      console.log("attachment:", filename, mimetype, encoding)
      const stream = createReadStream();
      const resolvedTo =   new Promise((resolve,reject) =>{
        stream.on('end', () => {console.log("ReadStream Ended")})
          .on('close', () => {console.log("ReadStream Closed")})
          .on('error', (err) => {console.error("ReadStream Error",err)})
        .pipe(createWriteStream(`./upload/${filename}`))
          .on('end', () => {
            console.log("WriteStream Ended");
            resolve("end")
          })
          .on('close', () => {
            console.log("WriteStream Closed");
            resolve("close")
          })
          .on('error',(err) => {
            console.log("WriteStream Error",err);
            reject("error")
          });
      });

      
      return this.issueService.createOne({
      data: {
        createdBy: {
          connect: {
            id: ctx.user.id
          }
        },
        currentColumn: {
          connect: {
            id: data.currentColumnId
          }
        },
        assignedTo: data.assignedToId ? {
          connect: {
            id: data.assignedToId
          }
        } : undefined,
        attachedFiles: undefined,
        ...data 
      }
    });
  }

  // => Updates issue by id
  @Mutation(() => Issue, { description: 'Updates issue by id' })
  async updateTodo(
    @Args('UpdateIssueInput') data: UpdateIssueInput,
  ): Promise<Issue> {
    return this.issueService.updateOne({
      where: {
        id: data.id 
      },
      data: {
        ...data,
        attachedFiles: undefined
      }
    });
  }

  // => Deletes issue by id
  @Mutation(() => Issue, { description: 'Deletes issue by id' })
  async deleteTodo(
    @Args('DeleteIssueInput') { id }: IssueByIdArgs,
  ): Promise<Issue> {
    return this.issueService.deleteOne({
      id: id,
    });
  }


}
