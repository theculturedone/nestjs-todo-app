import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Issue, Prisma } from '@prisma/client';

@Injectable()
export class IssueService {
  constructor(private prisma: PrismaService) {}


  async getOneOrFail(
    issueWhereUniqueInput: Prisma.IssueWhereUniqueInput,
  ): Promise<Issue | null> {
    return await this.prisma.issue.findUniqueOrThrow({
      where: issueWhereUniqueInput,
    });
  }

  async getOne(
    issueWhereUniqueInput: Prisma.IssueWhereUniqueInput,
  ): Promise<Issue | null> {
    return await this.prisma.issue.findUnique({
      where: issueWhereUniqueInput,
    });
  }

  async getMany(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.IssueWhereUniqueInput;
    where?: Prisma.IssueWhereInput;
    orderBy?: Prisma.IssueOrderByWithRelationInput;
  }): Promise<Issue[]> {
    const { skip, take, cursor, where, orderBy } = params;

    return await this.prisma.issue.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createOne(params: { data: any }): Promise<Issue> {
    const { data } = params;
    return await this.prisma.issue.create({
      data: data,
    });
  }

  async updateOne(params: {
    where: Prisma.IssueWhereUniqueInput;
    data: Prisma.IssueUpdateInput;
  }): Promise<Issue> {
    const { where, data } = params;
    return await this.prisma.issue.update({
      data,
      where,
    });
  }

  async deleteOne(where: Prisma.IssueWhereUniqueInput): Promise<Issue> {
    return await this.prisma.issue.delete({
      where,
    });
  }

  
}
