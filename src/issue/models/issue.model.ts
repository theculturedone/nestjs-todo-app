import { Field, ObjectType, ID, Int } from '@nestjs/graphql'
// TODO: import IssueColumn from IssueColumn.model.ts
// TODO: import User from User.model.ts
import { User } from 'src/user/models/user.model'
import { IssueColumn } from 'src/issueColumn/models/issueColumn.model'
import { S3StoredFile } from 'src/s3StoredFile/models/s3StoredFile.model'

@ObjectType({ description: 'Simple Issue Model' })
export class Issue {
  @Field(() => ID, { nullable: true })
  id: string

  @Field(
    () => String, { 
    nullable: true, 
    description: 'Unique code (inside this project) that will probably corespond to a branch name.'
    })
  identifierCode?: string

  @Field(
    () => String, {
      nullable: true,
      description: 'Issue description. Will also probably contain links to s3 pictures or something like that.',
    })
  description?: string
 
  @Field(
    () => String, {
      nullable: true,
      description: 'Issue description in human terms. For showing the client what is being worked on (in words that he can understand).',
    })
  humanTermsDescription?: string

  @Field(
    () => Int, {
      nullable: true,
      description: 'Issue priority. 1-n (1 is highest priority). Helps in filtering by priority.',
    })
  priority?: number

  @Field(
    () => IssueColumn, {
      nullable: true,
      description: 'Issue current column. A project has multiple columns (ex. todo, tested, done). Each column has issues currently in it.',
    })
  currentColumn?: IssueColumn
 
    
  @Field(
    () => User, {
      nullable: true,
      description: 'The person who created the issue.',
    })
  createdBy?: User
  
  @Field(
    () => User, {
      nullable: true,
      description: 'The person assigned to this issue.',
    })
  assignedTo?: User
  
  @Field(
    () => S3StoredFile, {
      nullable: true,
      description: 'Files attached to this issue(images/pdfs)'
    }
  )
    
  @Field(() => Date, { nullable: true })
  created_at?: Date

  @Field(() => Date, { nullable: true })
  updated_at?: Date
}
