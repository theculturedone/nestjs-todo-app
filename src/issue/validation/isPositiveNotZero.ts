import { Injectable } from "@nestjs/common";
import { registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";

@Injectable()
@ValidatorConstraint({ name: 'IsPositiveNotZero' })
export class IsPositiveNotZeroRule implements ValidatorConstraintInterface {

  async validate(value: number) {
    if(value >= 0) {
      return true
    }

    return false 
  
  }

  defaultMessage() {
    return `Issue doesn't exist`;
  }
}

export function IsLargerThanZero(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IssueExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsPositiveNotZeroRule,
    });
  }
}