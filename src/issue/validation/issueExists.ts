import { Injectable } from "@nestjs/common";
import { registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";
import { IssueService } from "../issue.service";

@Injectable()
@ValidatorConstraint({ name: 'IssueExists', async: true })
export class IssueExistsRule implements ValidatorConstraintInterface {
  constructor(private issueService: IssueService) {}

  async validate(value: string) {
    try {
      await this.issueService.getOneOrFail({
        id: value
      });
    } catch (e) {
      return false;
    }

    return true;
  }

  defaultMessage() {
    return `Issue doesn't exist`;
  }
}

export function IssueExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IssueExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IssueExistsRule,
    });
  }
}