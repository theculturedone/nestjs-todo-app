
import { Module } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { IssueColumnService } from "./issueColumn.service";
import { IssueColumn } from "./models/issueColumn.model";
import { Issue } from "src/issue/models/issue.model";
import { Project } from "src/project/models/project.model";
@Module({
    imports: [ Issue, Project, IssueColumn],
    providers: [ PrismaService, IssueColumnService ],
    exports: [ IssueColumnService, IssueColumn ],
    
})
export class IssueColumModule {}