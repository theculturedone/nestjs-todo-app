import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { IssueColumn, Prisma } from '@prisma/client';

@Injectable()
export class IssueColumnService {
  constructor(private prisma: PrismaService) {}

  async getOneOrFail(
    issueColumnWhereUniqueInput: Prisma.IssueColumnWhereUniqueInput,
  ): Promise<IssueColumn | null> {
    return await this.prisma.issueColumn.findUniqueOrThrow({
      where: issueColumnWhereUniqueInput,
    });
  }
}
