import { Field, ObjectType, ID } from '@nestjs/graphql';
// TODO: import Issue from issue graphql model
import { Issue } from 'src/issue/models/issue.model';
import { Project } from 'src/project/models/project.model';

@ObjectType({ description: 'Describes a column of issues (ex: todo/done/tested)' })
export class IssueColumn {
  @Field(() => ID)
  id: string

  @Field(
    () => String, { 
    nullable: true, 
    description: 'Name of the column.'
    })
  name?: string

  @Field(
    () => Project, {
      nullable: true,
      description: 'The project this issue belongs to.',
    })
  project?: Project
 
  @Field(
    () => [Issue], {
      nullable: true,
      description: 'Issues currently in this issueColumn.',
    })
  issues?: Issue[]

    
  @Field(() => Date, { nullable: true })
  created_at?: Date

  @Field(() => Date, { nullable: true })
  updated_at?: Date
}
