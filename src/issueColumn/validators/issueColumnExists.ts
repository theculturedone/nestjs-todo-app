import { Injectable } from "@nestjs/common";
import { registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";
import { IssueColumnService } from "../issueColumn.service";

@Injectable()
@ValidatorConstraint({ name: 'IssueColumnExists', async: true })
export class IssueColumnExistsRule implements ValidatorConstraintInterface {
  constructor(private issueColumnService: IssueColumnService) {}

  async validate(value: string) {
    try {
      await this.issueColumnService.getOneOrFail({
        id: value
      });
    } catch (e) {
      return false;
    }

    return true;
  }

  defaultMessage() {
    return `Issue column doesn't exist`;
  }
}

export function IssueColumnExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IssueExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IssueColumnExistsRule,
    });
  }
}