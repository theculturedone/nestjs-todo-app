import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { useContainer } from 'class-validator';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // we need to useContainer so there is no wierd dependency problem with our services and validators
  // see https://dev.to/avantar/custom-validation-with-database-in-nestjs-gao for reference
  useContainer(app.select(AppModule), { fallbackOnErrors: true })
  app.useGlobalPipes(new ValidationPipe())
  await app.listen(4000);
}

bootstrap();
