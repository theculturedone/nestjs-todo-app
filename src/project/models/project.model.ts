import { Field, ObjectType, ID } from '@nestjs/graphql'
import { User } from 'src/user/models/user.model'
import { IssueColumn } from 'src/issueColumn/models/issueColumn.model'

@ObjectType({ description: 'Project Model' })
export class Project {
  @Field(() => ID, { nullable: true })
  id: string

  @Field(
    () => String, {
    nullable: true, 
    description: 'Name of the project.'
    })
  name?: string

  @Field(
    () => [User], {
      nullable: true,
      description: 'Users that are currently part of this project.',
    })
  usersWorkingOnProject?: User[]
 
  @Field(
    () => [IssueColumn], {
      nullable: true,
      description: 'Columns that contain issues that are currently set to that column. (ex: todo/tested/done/merged)',
    })
  columns?: IssueColumn[]

  @Field(() => Date, { nullable: true })
  created_at?: Date

  @Field(() => Date, { nullable: true })
  updated_at?: Date
}
