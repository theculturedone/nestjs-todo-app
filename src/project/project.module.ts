
import { Module } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { ProjectService } from "./project.service";
import { Project  } from "./models/project.model";
import { IssueColumModule } from "src/issueColumn/issueColumn.module";
import { UserModule } from "src/user/user.module";

@Module({
    imports: [ IssueColumModule, UserModule ],
    providers: [ PrismaService ],
    exports: [ ProjectService, Project ],

})
export class ProjectModule {}