import { Field, ObjectType, ID } from '@nestjs/graphql';
// TODO: import Issue from issue graphql model
import { Issue } from 'src/issue/models/issue.model';

@ObjectType({ description: 'Describes an uploaded to s3 file.' })
export class S3StoredFile {
  @Field(() => ID)
  id: string

  @Field(
    () => Issue, { 
    nullable: true, 
    description: 'The issue this file belongs to.'
    })
  issue?: Issue

  @Field(
    () => String, {
      nullable: true,
      description: 'The format of the file (.png, .jpg, etc...)',
    })
  fileExtension?: string
 
  @Field(
    () => String, {
      nullable: true,
      description: 'The link to the file on s3.',
    })
  link?: string

  @Field(
    () => String, {
      nullable: true,
      description: 'The key of the resource on s3. Will be used when deleting the item. Shouldn t matter for frontend.',
    })
  key?: string 

    
  @Field(() => Date, { nullable: true })
  created_at?: Date

  @Field(() => Date, { nullable: true })
  updated_at?: Date
}
