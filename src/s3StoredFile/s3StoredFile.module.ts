
import { Module } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { S3StoredFileService } from "./s3StoredFile.service";
import { S3StoredFile } from "./models/s3StoredFile.model";
import { Issue } from "src/issue/models/issue.model";

@Module({
    imports: [ Issue, S3StoredFile],
    providers: [ PrismaService, S3StoredFileService],
    exports: [ S3StoredFileService, S3StoredFile],
    
})
export class S3StoredFileModule {}