import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { S3StoredFile, Prisma } from '@prisma/client';

@Injectable()
export class S3StoredFileService {
  constructor(private prisma: PrismaService) {}

  async getOneOrFail(
    s3StoredFileWhereUniqueInput: Prisma.S3StoredFileWhereUniqueInput,
  ): Promise<S3StoredFile | null> {
    return await this.prisma.s3StoredFile.findUniqueOrThrow({
      where: s3StoredFileWhereUniqueInput,
    });
  }
  
}
