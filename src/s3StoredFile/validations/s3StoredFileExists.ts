import { Injectable } from "@nestjs/common";
import { registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";
import { S3StoredFileService } from "../s3StoredFile.service";

@Injectable()
@ValidatorConstraint({ name: 'S3StoredFileExists', async: true })
export class S3StoredFileExistsRule implements ValidatorConstraintInterface {
  constructor(private s3StoredFileService: S3StoredFileService) {}

  async validate(value: string) {
    try {
      await this.s3StoredFileService.getOneOrFail({
        id: value
      });
    } catch (e) {
      return false;
    }

    return true;
  }

  defaultMessage() {
    return `S3StoredFiles doesn't exist.`;
  }
}

export function S3StoredFileExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'S3StoredFileExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: S3StoredFileExistsRule,
    });
  }
}