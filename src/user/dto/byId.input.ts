// => nestjs TypeGraphql
import { Field, ID, InputType } from '@nestjs/graphql';

// => common class-validatior validations
import {
  IsNotEmpty,
  IsUUID 
} from 'class-validator';

// => our costum validation rules 
import { UserExists } from "../validation/userExists"


@InputType('UserByIdInput', { description: 'Also ensures that the user with the provided id exists.'})
export class UserByIdArgs{
  @UserExists()
  @IsUUID()
  @IsNotEmpty()
  @Field(() => ID)
  id: string;

}