// => nestjs TypeGraphql
import { Field, InputType } from '@nestjs/graphql';

// => prisma types
import { Prisma } from "@prisma/client"

// => common class-validatior validations
import {
  IsEmail,
  IsNotEmpty,
  MaxLength, MinLength
} from 'class-validator';

// => our costum validation rules 
import { UserWithEmailDoesNotExist } from "../validation/userEmailIsUnique"


@InputType('CreateUserInput', { 
  description: 'Used in user creation'})
export class CreateUserInput {
  @UserWithEmailDoesNotExist()
  @IsEmail()
  @IsNotEmpty()
  @Field(() => String)
    email: string


  @IsNotEmpty()
  @Field(() => String)
    displayName: string
  
  }
