// => nestjs TypeGraphql
import { Field, ID, InputType, PartialType } from '@nestjs/graphql';

// => input types 
import { CreateUserInput } from './create.input';

// => common class-validatior validations
import {
  IsUUID,
} from 'class-validator';

// => our costum validation rules 
import { UserExists } from "../validation/userExists"


// ! we only specify the fields that are extra compared to the CreateTodoInput and we set the ones that are present
// inside the CreateTodoInput to optional by using PartialType(CreateTodoInput)
// less code better :D
@InputType('UpdateUserInput', { description: 'Used in User updating'})
export class UpdateUserInput extends PartialType(CreateUserInput){
  
  @UserExists()
  @IsUUID()
  @Field(() => ID, { nullable: false })
    id: string;

  }
