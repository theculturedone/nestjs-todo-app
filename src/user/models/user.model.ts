import { Field, ID, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class User {
  @Field(() => ID)
    id: string;

  @Field(() => String)
    email: string;

  @Field(() => String, { nullable: true })
    password?: string

  @Field(() => Date, { nullable: true })
    created_at?: Date;

  @Field(() => Date, { nullable: true })
    updated_at?: Date;
}