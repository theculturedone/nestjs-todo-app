
import { Module } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { UserResolver } from "./user.resolver";
import { UserService } from "./user.service";
import { User } from "./models/user.model";
@Module({
    imports: [ User ],
    providers: [PrismaService, UserService, UserResolver],
    exports: [UserService, User],
    
})
export class UserModule {}