import { Resolver, Query, Args, Mutation } from "@nestjs/graphql"
import { UserByIdArgs } from "./dto/byId.input"
import { CreateUserInput } from "./dto/create.input"
import { UpdateUserInput } from "./dto/update.input"

import { User } from "./models/user.model";
import { UserService } from "./user.service";

@Resolver(() => User)
export class UserResolver {
    constructor(private readonly userService: UserService) {}

    @Query(() => User, { description: "Gets self user if logged in else gets " })
    async getUser(user: User): Promise<User | null> {
        if(user) return user
        return null
    }

    @Query(() => [User], { description: "Gets a list of all the users" })
    async getUsers(): Promise<User[]> {
        return this.userService.users({})
    }

    @Mutation(() => User, { description: "Creates a new user" })
    async createUser(@Args('CreateUserInput') createUserData: CreateUserInput): Promise<User | null> {
        return this.userService.createUser({
          data: createUserData
        })
    }

    @Mutation(() => User, { description: "Updates existing user" })
    async updateUser(@Args('UpdateUserInput') updateUserData: UpdateUserInput): Promise<User | null> {
        return this.userService.updateUser({
          where: {
            id: updateUserData.id
          },
          data: updateUserData
        })
    }

    @Mutation(() => User, { description: "Delete user by id" })
    async deleteUser(@Args('UserByIdArgs') { id }: UserByIdArgs): Promise<User | null> {
        return this.userService.deleteUser({
          id: id
        })
    }


}