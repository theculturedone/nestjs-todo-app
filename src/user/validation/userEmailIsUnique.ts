import { Injectable } from "@nestjs/common";
import { registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";
import { UserService } from "../user.service";

@Injectable()
@ValidatorConstraint({ name: 'UserWithEmailDoesNotExist', async: true })
export class UserWithEmailDoesNotExistRule implements ValidatorConstraintInterface {
  constructor(private userService: UserService) {}

  async validate(value: string) {
    try {
      await this.userService.userOrFail({
        email: value
      });
    } catch (e) {
      return false;
    }

    return true;
  }

  defaultMessage() {
    return `User with this email already exists. Please provide a valid one.`;
  }
}

export function UserWithEmailDoesNotExist(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UserWithEmailDoesNotExist',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UserWithEmailDoesNotExistRule,
    });
  }
}